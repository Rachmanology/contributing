Thanks for considering a contribution to GitLab's
[built-in project templates](https://docs.gitlab.com/ee/gitlab-basics/create-project.html#built-in-templates).

### Contributing a new project template

If you'd like to contribute a new built-in project template to be distributed
with GitLab, please do the following:

1. Create a new public project with the project content you'd like to contribute in the [project-templates](https://gitlab.com/gitlab-org/project-templates) group. You can view a working example [here](https://gitlab.com/gitlab-org/project-templates/dotnetcore).
   * Projects should be as simple as possible and free of any unnecessary assets or dependencies.
1. When the project is ready for review, please create a new issue in [gitlab-ce](https://gitlab.com/gitlab-org/gitlab-ce/issues/) and link to your project.

To make the project template available when creating a new project, the vendoring process will have to be completed:

1. Create a working template ([example](https://gitlab.com/gitlab-org/project-templates/dotnetcore)) and export
1. Add boilerplate to `gitlab-ce/lib/gitlab/project_template.rb`, `gitlab-ce/spec/lib/gitlab/project_template.rb`, and `gitlab-ce/app/assets/javascripts/projects/project_new.js`. See MR [!25486](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/25486) for an example
1. Add exported project to `gitlab-ce/vendor/project_templates` after running the [bundle_repo](/bundle_repo) script
1. Run `bin/rake gettext:regenerate` in the `gitlab-ce` project and commit new `.pot` file
1. Add changelog (e.g. `bin/changelog -m 25486 "Add Project template for .NET"``)
1. Add icon to https://gitlab.com/gitlab-org/gitlab-svgs, as show in [this example](https://gitlab.com/gitlab-org/gitlab-svgs/merge_requests/195)
1. Run `yarn run svgs` on `gitlab-svgs` project and commit result
1. Forward changes in `gitlab-svgs` project to master
1. Rebase master to pick up new svgs
1. Test everything is working

### Contributing an improvement to an existing template

Existing templates are available in the [project-templates](https://gitlab.com/gitlab-org/project-templates)
group.

To contribute a change, please open a merge request in the relevant project
and mention @jeremy when you are ready for a review.
